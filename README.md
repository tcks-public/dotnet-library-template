# dotNET Library Template

Template project for .NET library project.

## Getting started

* Fork/copy this repository
* Run `./init-script.sh MyProjectName`
* Done!

## Template contains
* Solution file
  * Class library project
  * MSTest project
    * CodeCoverage
* CI/CD configuration
  * Builds for Debug/Release configuration
  * Tests for Debug/Release configuration
    * With code coverage collection
