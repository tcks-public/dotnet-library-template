LIB_PROJ_NAME=$1
LIB_PROJ_PATH="${LIB_PROJ_NAME}/${LIB_PROJ_NAME}.csproj"

TEST_PROJ_NAME=$1.TESTS
TEST_PROJ_PATH="${TEST_PROJ_NAME}/${TEST_PROJ_NAME}.csproj"

# moving to directory with source code
cd src

# new solution file
dotnet new sln --name $1

# new library project file for solution
dotnet new classlib --name $LIB_PROJ_NAME
dotnet sln add $LIB_PROJ_PATH

# new test project file for solution
dotnet new mstest --name $TEST_PROJ_NAME
dotnet add $TEST_PROJ_PATH reference $LIB_PROJ_PATH
dotnet sln add $TEST_PROJ_PATH

# add nuget packages to projects
dotnet add $TEST_PROJ_PATH package "coverlet.collector"
dotnet add $TEST_PROJ_PATH package "coverlet.msbuild"
dotnet add $TEST_PROJ_PATH package "JunitXml.TestLogger"

# moving back to root direcotry
cd ..